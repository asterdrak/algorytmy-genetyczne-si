#!/usr/bin/ python3
#-*- coding: utf-8 -*-

import string
import random

class Member:
    """ sets maximum adapt grade """
    max_adapt_grade = 100

    def __init__(self, pattern_length, pattern = None):
        if pattern == None:
            self.sentence = self.__random_string__(pattern_length)
        else:
            self.sentence = pattern

        self.adapt_grade = 0
        self.__init_iterable__()

    def __str__(self):
        """ object returns sentence when casted to string """
        return self.sentence

    def __random_string__(self, length):
        """ generates random string with specific length """
        all_signs = self.__get_all_signs__()
        sentence = ''
        for i in range(length):
            sentence += random.choice(all_signs)
        return sentence

    def __get_all_signs__(self):
        s = ''
        for i in range(32, 126):
            s += chr(i)
        return list(s)

    def __iter__(self):
            self.list = [self.sentence, self.adapt_grade]
            return self
    # ------------------------------------------------
    """ iterable """
    def __init_iterable__(self):
        self.__current__ = 0

    def __next__(self):
            if self.__current__ >= len(self.list):
                    self.__current__ = 0
                    raise StopIteration
            else:
                    self.__current__ += 1
                    return self.list[self.__current__ - 1]
    # ------------------------------------------------

    # public:
    def length(self):
        return len(self.sentence)

    def set_adapt_grade(self, new_adapt_grade):
        self.adapt_grade = new_adapt_grade

# ---------------------------------------------------------------------------------------------
# ---------------------------------------------------------------------------------------------
# ---------------------------------------------------------------------------------------------

class Pattern(Member):
    def __init__(self, sentence):
        """gets destiny sentence as initial value"""
        # Member.__init__(self)
        self.sentence = sentence
        self.adapt_grade = Member.max_adapt_grade

    def compare(self, compare_sentence):
        if(len(self.sentence) != len(compare_sentence)):
            raise Exception('Bad length of member sentence')

        def string_to_int_list(string):
            return list(map(ord, string))

        def absolute_difference(x, y):
            return abs(x - y)

        return sum(list(map(lambda 
            pattern_letter, sentence_letter: absolute_difference(sentence_letter, pattern_letter),
            string_to_int_list(compare_sentence), string_to_int_list(self.sentence)
            )))