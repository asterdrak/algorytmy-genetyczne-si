#!/usr/bin/ python3
#-*- coding: utf-8 -*-

from .members import Member

class Population:
  """docstring for Population"""
  def __init__(self, Base):
    self.Base = Base
    self.list = []
    self.__init_iterable__()

  def createNew(self):
    for i in range(self.Base.max_population_lenght):
      new_member = Member(self.Base.pattern.length())
      new_member.set_adapt_grade(self.Base.pattern.compare(new_member.sentence))
      self.list.append(new_member)

  def createFromList(self, population):
    self.list = population

  def create(self, population = None):
    if self.list != []:
      raise Exception('Cannot create population, already done')
    elif population == None:
      self.createNew()
    else:
      self.createFromList(population)

  def add_member(self, member):
    self.list.append(member)

  def sort(self):
    self.list = sorted(self.list, key=lambda member: member.adapt_grade)

  def __iter__(self):
          return self
  # ------------------------------------------------
  """ iterable """
  def __init_iterable__(self):
      self.__current__ = 0

  def __next__(self):
          if self.__current__ >= len(self.list):
                  self.__current__ = 0
                  raise StopIteration
          else:
                  self.__current__ += 1
                  return self.list[self.__current__ - 1]
  # ------------------------------------------------