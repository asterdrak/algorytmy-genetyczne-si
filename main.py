
#!/usr/bin python3
#-*- coding: utf-8 -*-

from classes import *
import random

class Base:
  max_population_lenght = 2048
  max_iteriation_number = 200
  elitary_factor = 0.05
  mutation_probability = 0.2
  crossing_subset = max_population_lenght // 4
  pattern = ''


Base.pattern = members.Pattern("I love AI")
print("Destiny sentence: ", Base.pattern)

current_population = population.Population(Base)
current_population.create()
current_population.sort()

end_interval = int(Base.max_population_lenght*Base.elitary_factor)

for i in range(Base.max_iteriation_number):
  alive_members = list(current_population)[0:end_interval]
  new_population = population.Population(Base)
  new_population.create(alive_members)

  for i in range(len(list(new_population)), Base.max_population_lenght):
    crossing_members = [random.choice(list(list(current_population)[0:Base.crossing_subset])) for i in range(2)]
    crossing_axis = random.randint(0, len(str(Base.pattern)))
    new_member_sentence = str(crossing_members[0])[:crossing_axis] + str(crossing_members[1])[crossing_axis:]

    if(random.randint(0, 100)/100 < Base.mutation_probability):
      random_letter = random.choice(members.Member.__get_all_signs__(None))
      new_member_sentence_list = list(new_member_sentence)
      new_member_sentence_list[random.randint(0, len(new_member_sentence_list) - 1)] = random_letter
      new_member_sentence = ''.join(new_member_sentence_list)

    new_member = members.Member(Base.pattern.length(), new_member_sentence)
    new_member.set_adapt_grade(Base.pattern.compare(new_member.sentence))
    new_population.add_member(new_member)

  new_population.sort()

  print(list(list(new_population)[0]))

  if current_population.list[0].adapt_grade == 0: break;

  current_population = new_population
  new_population = None
